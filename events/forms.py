from drigan.forms import DriganModelForm
from events.models import Event, Organizer, Attraction
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class AddEventForm(DriganModelForm):
    error_css_class = "form-error"

    class Meta:
        model = Event
        fields = ('name', 'edition', 'category')


class EditEventForm(DriganModelForm):
    class Meta:
        model = Event
        fields = ('name', 'edition', 'start_date', 'end_date',
                  'website', 'category', 'description')


class AddOrganizerForm(DriganModelForm):
    class Meta:
        model = Organizer
        fields = ('name', 'mail', 'phone')


class EditOrganizerForm(DriganModelForm):
    class Meta:
        model = Organizer
        fields = ('name', 'mail', 'phone', 'address')


class AddAttractionForm(DriganModelForm):
    class Meta:
        model = Attraction
        fields = ('name', 'start_date', 'end_date', 'place',
                  'description', 'category', 'registered_only', 'confirm_registration')

    def clean_confirm_registration(self):
        if not self.cleaned_data.get('registered_only') and self.cleaned_data.get('confirm_registration'):
            raise ValidationError(_("Confirmation can be used only if \'registered only\' is chosen"))
        return self.cleaned_data.get('confirm_registration')


class ChangeEventLogoForm(DriganModelForm):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("label_suffix", "")
        super(ChangeEventLogoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Event
        fields = ('logo',)
        labels = {
            'logo': _('Change logo')
        }
