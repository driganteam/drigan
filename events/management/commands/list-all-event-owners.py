# -*- coding: utf-8 -*-
import csv
import sys
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):

        users = []

        for u in get_user_model().objects.all():

            if not (u.has_perm('events.add_event') or u.is_superuser):
                continue

            events = [e.name for e in u.events.all()]

            users.append([u, events])

        rows = []

        for u, events in users:
            row = []
            row.append(u.first_name)
            row.append(u.last_name)
            row.append(u.email)
            row.append(u.username)
            row.extend(events)
            rows.append(row)

        w = csv.writer(sys.stdout)
        w.writerows(rows)
        #
        # print(rows)




