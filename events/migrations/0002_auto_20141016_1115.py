# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attraction',
            name='end_date',
            field=models.DateTimeField(help_text='Please provide date and time in format %Y-%m-%d %H:%M:%S, for example 1985-09-19 21:30:00.\n    You may omit time part', verbose_name='end date'),
        ),
        migrations.AlterField(
            model_name='attraction',
            name='start_date',
            field=models.DateTimeField(help_text='Please provide date and time in format %Y-%m-%d %H:%M:%S, for example 1985-09-19 21:30:00.\n    You may omit time part', verbose_name='start date'),
        ),
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(null=True, help_text='Please provide date and time in format %Y-%m-%d %H:%M:%S, for example 1985-09-19 21:30:00.\n    You may omit time part', blank=True, verbose_name='end date of the event'),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(null=True, help_text='Please provide date and time in format %Y-%m-%d %H:%M:%S, for example 1985-09-19 21:30:00.\n    You may omit time part', blank=True, verbose_name='start date of the event'),
        ),
    ]
