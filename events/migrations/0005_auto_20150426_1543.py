# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations, transaction
import reversion


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_drop_old_models_20151020_1654'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attraction',
            options={'verbose_name_plural': 'attractions', 'verbose_name': 'attraction', 'ordering': ['start_date', 'name']},
        ),
        migrations.AlterModelOptions(
            name='event',
            options={'verbose_name_plural': 'events', 'verbose_name': 'event'},
        ),
        migrations.AlterModelOptions(
            name='organizer',
            options={'verbose_name_plural': 'organizers', 'verbose_name': 'organizer'},
        ),
        migrations.RemoveField(
            model_name='attraction',
            name='deleted_at',
        ),
        migrations.RemoveField(
            model_name='event',
            name='deleted_at',
        ),
        migrations.AlterField(
            model_name='attraction',
            name='end_date',
            field=models.DateTimeField(verbose_name='end date', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00. You may omit the time part.'),
        ),
        migrations.AlterField(
            model_name='attraction',
            name='start_date',
            field=models.DateTimeField(verbose_name='start date', help_text='Please provide date and time according to the following example 1985-09-19 21:30:00. You may omit the time part.'),
        ),
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(verbose_name='end date of the event', blank=True, help_text='Please provide date and time according to the following example 1985-09-19 21:30:00. You may omit the time part.', null=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(verbose_name='start date of the event', blank=True, help_text='Please provide date and time according to the following example 1985-09-19 21:30:00. You may omit the time part.', null=True),
        ),
    ]
