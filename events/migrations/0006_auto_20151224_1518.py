# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_auto_20150927_0932'),
    ]

    operations = [
        migrations.AddField(
            model_name='attraction',
            name='registered_only',
            field=models.BooleanField(default=False, verbose_name='Registered users only'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attraction',
            name='confirm_registration',
            field=models.BooleanField(verbose_name='Confirm user registration', default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attraction',
            name='end_date',
            field=models.DateTimeField(help_text='Dodaj datę w formacie zgodnym z tym przykładem: 1985-09-19 21:30:00. Możesz pominąć godzinę.', verbose_name='end date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attraction',
            name='start_date',
            field=models.DateTimeField(help_text='Dodaj datę w formacie zgodnym z tym przykładem: 1985-09-19 21:30:00. Możesz pominąć godzinę.', verbose_name='start date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attractioncategory',
            name='active',
            field=models.BooleanField(default=True, verbose_name='aktywny'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attractioncategory',
            name='name',
            field=models.CharField(max_length=100, verbose_name='nazwa'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(null=True, verbose_name='end date of the event', help_text='Dodaj datę w formacie zgodnym z tym przykładem: 1985-09-19 21:30:00. Możesz pominąć godzinę.', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(null=True, verbose_name='start date of the event', help_text='Dodaj datę w formacie zgodnym z tym przykładem: 1985-09-19 21:30:00. Możesz pominąć godzinę.', blank=True),
            preserve_default=True,
        ),
    ]
