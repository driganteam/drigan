��    V      �     |      x  0   y  +   �  @   �  -        E     I  	   X     b     x     �     �     �  '   �  )   �     	     0	     <	     H	  <   b	     �	     �	  '   �	     �	     �	  
   
     
  	   )
     3
  "   M
  $   p
     �
     �
     �
  !   �
     �
                 )   $  	   N     X     j     }     �     �     �  f   �     4     J     g          �     �  (   �     �     �  7   �          &  
   7     B     X     d     m     u     �     �     �     �     �     �     �  
   �                      	   %     /  
   >     I     V  
   \     g       @  �  1   �  2   �  B   -  A   p     �     �     �     �     �                7  $   F  '   k     �     �     �  %   �  W   �     C     T  %   f     �     �     �     �     �  !   �  &     )   8     b     �  #   �     �     �     �     �     �  8   �     .     :     N     b     ~     �     �  X   �  !        0     L     j     q     �     �     �     �  D   �     !     '     :     C     V  	   _     i     q     �     �     �     �     �     �  
   �     �  
   �     �                         0     >     M     U     f     {         D           U   B                         4             )                
      ;   0   6   A   P   L       7   >       @   I               =   K                      F       3   Q      C      !   8      -   *   $          9   .       J           /   O           2                         5      1      R   '   M      E               %             &           H      (       :                 <   V   ,   #   	   S         G             ?   N   T   +      "                
        Delete attraction named "%(name)s"
     
        Delete event named "%(name)s"
     
    Do you really want to delete attraction named: "%(name)s"?
 
   Do you really want to delete "%(name)s"?
 Add Add attraction Add event Add registration form Admin added successfully Administrators Administrators list Admins list: Attraction has been added successfully. Attraction has been changed successfully. Attraction has been deleted. Attractions Change logo Confirm user registration Confirmation can be used only if 'registered only' is chosen Delete attraction Delete event Done editing form, return to attraction Edit attraction Edit attraction: Edit event Edit event - %(event)s Edit form Enter email of new admin: Event has been added successfully. Event has been changed successfully. Event has been deleted. Form data incorrect. Logo changed successfully Logo not changed - no data given. Lp Name No upcoming events... None :( Only if event is cyclic (eg. "1", "2014") Organizer Organizer details Organizer details: Other editions of this event Owner deleted successfully Participants list Place Please provide date and time according to the following example {example}. You may omit the time part. Registered users only Return to attraction edition Return to event edition Save Save as csv Sign up for this attraction! There is no user with such email address Unknown Website Without edition (eg. "Long Race", not "Long Race 2013") address all participants attraction attraction categories attractions category creator creator's mail description of the attraction description of the event e-mail address edition name end date end date of the event event event name events everyone name none yet organizer organizer only organizers phone number place start date start date of the event website Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-02 21:17+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
        Usuń atrakcję o nazwie "%(name)s"
     
        Usuń wydarzenie o nazwie "%(name)s"
     
    Czy na pewno chcesz usunąć atrakcję o nazwie: "%(name)s"?
 
   Czy na pewno chcesz usunąć wydarzenie o nazwie "%(name)s"?
 Dodaj Dodaj atrakcję Dodaj wydarzenie Dodaj formularz rejestracyjny Pomyślnie dodano admina Administratorzy Lista administratorów Lista adminów Atrakcja została pomyślnie dodana. Atrakcja została pomyślnie zmieniona. Atrakcja została usunięta. Atrakcje Zmień logo Zatwierdzaj rejestrację użytkownika Opcja 'tylko zarejestrowani użytkownicy' jest niezbędna dla zatwierdzania zgłoszeń! Usuń atrakcję Usuń wydarzenie Zakończ edycję i wróć do atrakcji Edytuj atrakcję Edytuj atrakcję Edytuj wydarzenie Edytuj wydarzenie - %(event)s Edytuj formularz Podaj adres email nowego admina:  Wydarzenie zostało pomyślnie dodane. Wydarzenie zostało pomyślnie zmienione. Wydarzenie zostało usunięte. Niepoprawne dane Logo zostało pomyślnie zmienione. Logo nie zostało zmienione Nr Nazwa Brak wydarzeń... Brak :( Tylko jeśli wydarzenie jest cykliczne (np. "1", "2014") Organizator Detale organizatora Detale oranizatora: Inne edycje tego wydarzenia Pomyślnie usunięto admina Lista uczestników Miejsce Dodaj datę w formacie zgodnym z tym przykładem: {example}. Możesz pominąć godzinę. Tylko zarejestrowani użytkownicy Powróć do edycji atrakcji Powróć do edycji wydarzenia Zapisz Zapisz jako plik csv Zapisz się na to wydarzenie! Nie znaleziono użytkownika Nieznany Strona internetowa Bez nazwy edycji (np. "Długi Wyścig", a nie "Długi Wyścig 2013") adres wszyscy uczestnicy atrakcja kategorie atrakcji atrakcje kategoria twórca e-mail twórcy opis atrakcji opis wydarzenia adres e-mail nazwa edycji data końcowa koniec wydarzenia wydarzenie nazwa wydarzenia wydarzenia wszyscy nazwa brak organizator tylko organizator organizatorzy numer telefonu miejsce data początkowa początek wydarzenia strona internetowa 