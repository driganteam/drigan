from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.views.generic.list import ListView
from events.models import Event
from guardian.shortcuts import get_objects_for_user


class StartView(ListView):

    template_name = "start.html"
    context_object_name = 'upcoming_events_list'

    __query = """
    SELECT * FROM events_event as e WHERE
    EXISTS (
      SELECT 1 FROM events_attraction as a
      INNER JOIN dynamic_forms_dynamicform as df ON(
        df.can_be_filled AND df.content_type_id = %s AND df.object_id = a.id
      )
      WHERE a.event_id = e.id
    )
    ORDER BY (
      SELECT MIN(start_date) FROM events_attraction as oba
      WHERE oba.event_id = e.id
    ) DESC
    """

    def get_queryset(self):
        # all_events = Event.objects.order_by('-start_date')
        ct = ContentType.objects.get(app_label="events", model='attraction')
        current_events = Event.objects.raw(self.__query, params=[ct.pk])
        return current_events

    def get_context_data(self, *args, **kwargs):
        context = super(StartView, self).get_context_data(*args, **kwargs)
        user = self.request.user
        if user.is_authenticated():
            my_events = get_objects_for_user(user, 'events.change_event', None, True, False, False) | user.events.all()
            context['user_events_list'] = my_events
        return context
