
from .settings_shared import *
from .settings_utils import *

DEBUG = get_env_bool('DRIGAN_DEBUG', False)
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
    ('Jacek Bzdak', 'jbzdak@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': get_env('DRIGAN_DATABASE_NAME', 'drigan'),
        # The following settings are not used with sqlite3:
        'USER': get_env('DRIGAN_DATABASE_USER_NAME', 'drigan'),
        'PASSWORD': get_env('DRIGAN_DATABASE_USER_PASSWORD', None),
        'HOST':  get_env('DRIGAN_DATABASE_HOST', ''),
        'PORT': get_env('DRIGAN_DATABASE_PORT', 5432, int),
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = get_string_list_env('DRIGAN_ALLOWED_HOSTS')

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = get_env('TIME_ZONE', 'Europe/Warsaw')

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = get_env('LANG_CODE', 'pl')

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = get_env('MEDIA_ROOT')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = get_env('MEDIA_URL', '/media/')

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = get_env('STATIC_ROOT')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = get_env('STATIC_URL', '/static/')

DEFAULT_FROM_EMAIL=SERVER_EMAIL=get_env('SERVER_EMAIL')

EMAIL_BACKEND = get_env('EMAIL_BACKEND', 'django.core.mail.backends.smtp.EmailBackend')
EMAIL_FILE_PATH = get_env('EMAIL_FILE_PATH', None)

EMAIL_HOST = get_env('SMTP_SERVER_EMAIL_HOST', None)
EMAIL_PORT = get_env('SMTP_SERVER_PORT', '587')
EMAIL_USE_TLS = get_env_bool('SMTP_SERVER_TLS', False)
EMAIL_HOST_USER = get_env('SMTP_SERVER_USERNAME', get_env('SERVER_EMAIL'))
EMAIL_HOST_PASSWORD = get_env('SMTP_SERVER_PASSWORD', None)

DYNFORM_PRIVATE_MEDIA_ROOT= get_env('DRIGAN_PRIVATE_MEDIA_LOCATION')

DYNFORM_PRIVATE_FILE_STORAGE = get_env('DRIGAN_PRIVATE_MEDIA_STORAGE', 'dynamic_forms.example_file_storage.DynamicFormsPrivateStorage')

# Make this unique, and don't share it with anybody.
SECRET_KEY = get_env('SECRET_KEY')

LOCALE_PATHS = get_string_list_env('DRIGAN_LOCALE_PATHS')


