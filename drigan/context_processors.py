from django.conf import settings


def events_settings(request):
    return {
        'DRIGAN_CREATE_EVENTS_WITHOUT_PERMISSION': settings.DRIGAN_CREATE_EVENTS_WITHOUT_PERMISSION,
    }
