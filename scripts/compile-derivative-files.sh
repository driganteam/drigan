#!/usr/bin/env bash

ROOT=$(pwd)

cd events

${ROOT}/manage.py compilemessages

cd ${ROOT}/dynamic_forms

${ROOT}/manage.py compilemessages

cd ${ROOT}

lessc -x --clean-css drigan/static/less/style.less drigan/static/style.css

