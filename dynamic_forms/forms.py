from django import forms
from django.utils.text import slugify
from dynamic_forms.fieldtype import get_field_type_choices
from dynamic_forms.models import DynamicFormField, DynamicForm
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class DynamicFormForm(forms.Form):
    error_css_class = "form-error"
    required_css_class = 'form-required'


class DynamicModelFormForm(forms.ModelForm):
    error_css_class = "form-error"
    required_css_class = 'form-required'


class AddDynamicFormField(DynamicModelFormForm):

    field_type = forms.ChoiceField(
        get_field_type_choices(),
        label=_("field type"),
    )

    class Meta:
        model = DynamicFormField
        fields = ('name', 'field_type', 'required')


class AddChoices(DynamicFormForm):

    name = forms.CharField(max_length=100, label=_("name"))


class BaseDynamicForm(DynamicFormForm):

    def __init__(self, dynamic_form, *args, **kwargs):
        super(BaseDynamicForm, self).__init__(*args, **kwargs)
        dynamic_fields = dynamic_form.fields
        for dynamic_field in dynamic_fields.all():
            self.fields[slugify(dynamic_field.name)] = dynamic_field.get_django_field()


class ChangeDynamicFormPropertiesForm(DynamicModelFormForm):

    def __init__(self, *args, **kwargs):
        super(ChangeDynamicFormPropertiesForm, self).__init__(*args, **kwargs)
        if settings.DRIGAN_CAN_CHANGE_LIST_VISIBILITY:
            self.fields['list_visibility'] = forms.ChoiceField(
                choices=self.instance._meta
                .get_field_by_name('list_visibility')[0].choices)
        else:
            del self.fields['list_visibility']

    class Meta:
        model = DynamicForm
        fields = ('can_be_filled', 'list_visibility',)
