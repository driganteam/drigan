from django.forms import Textarea
import hashlib
from os import urandom
import binascii
import json

from django import forms
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy, ugettext
from django.utils.html import format_html
from django.forms.fields import ChoiceField, FileField
from django.core.exceptions import ValidationError

from dynamic_forms.fieldtype import create_dynamic_field_from_django_form, \
    _DjangoDynamicFieldController, register_field_type, BaseWrapper
from dynamic_forms.models import DynamicFormFile
from dynamic_forms.utils.file_storage import DriganFileStorage


create_dynamic_field_from_django_form(forms.IntegerField, ugettext_lazy('Number Field'))
create_dynamic_field_from_django_form(forms.CharField, ugettext_lazy('Short text Field'))
create_dynamic_field_from_django_form(forms.EmailField, ugettext_lazy('E-mail Field'))
create_dynamic_field_from_django_form(forms.CharField, ugettext_lazy('Date Field (Temporarily used char field)'), 'DynamicDateField')
# FIXES REJCEN-245
create_dynamic_field_from_django_form(forms.BooleanField, ugettext_lazy('Yes/No Field'))


class DynamicFileFieldDataWrapper(BaseWrapper):
    def as_text(self):
        if self.data is None:
            return ugettext_lazy("No file attached")
        f = DynamicFormFile.objects.get(pk=self.data)
        return f.file_name_for_user

    def as_html(self):
        """
        Displays the link to download file
        """
        if self.data is None:
            return ugettext_lazy("No file attached")
        f = DynamicFormFile.objects.get(pk=self.data)
        return format_html("<a href={0}>{1}</a>",
                           reverse('dynamic_forms.views.serve_file',
                                   kwargs={'pk': self.data}),
                           f.file_name_for_user)


@register_field_type('DynamicFileField')
class DynamicFileField(_DjangoDynamicFieldController):

    DESCRIPTION = ugettext_lazy('Attachment field (any file type)')
    DJANGO_FIELD_TYPE = forms.FileField
    WRAPPER = DynamicFileFieldDataWrapper

    def _create_field(self):
        field =  super()._create_field()
        def test_validator(value):
            if len(value.name) > 2000:
                raise ValidationError(ugettext_lazy("Don't joke please don't upload files with filenames longer than 2000 chars."))
        field.validators.append(test_validator)
        return field

    def __generate_file_sha(self, file):
        """
        This is crypto mumbo jumbo to add hard to guess names to files on disk
        (so even if someone will allow anonymous access to files these will
        be protected by links). Additionaly under some circumstances ``urandom``
        *might* be somewhat predicatble (due to poor random seed and almost no
        entropy on VM) we'll mix in django secret key which should have good
        entropy, as we pass it via KDF it shouldnt leek.

        :param file: file to turn transform
        :return: hex encoded random string
        """
        secret = settings.SECRET_KEY
        if isinstance(secret, str):
            secret = secret.encode(encoding='UTF-8', errors='strict')
        return binascii.b2a_hex(hashlib.pbkdf2_hmac('sha256', file.name.encode(encoding='UTF-8', errors='strict') + secret + urandom(16), urandom(16), 10000)).strip().decode('ascii')

    def serialize_field_content(self, name, uploaded_file, form, request, model):
        """
        Saves file to selected storage, and returns name of the file in this
        storage.
        """
        if uploaded_file is None:
            return None
        storage = DriganFileStorage()
        new_file_name = self.__generate_file_sha(uploaded_file)

        new_file_name = storage.save(new_file_name, uploaded_file)
        old_file_name = DynamicFormFile.clean_filename_for_user(uploaded_file.name)

        return DynamicFormFile.objects.create(
            data=model,
            name=name,
            file_name_in_storage=new_file_name,
            file_name_for_user=old_file_name,
            content_type=uploaded_file.content_type
        ).pk


class MIMECheckFileField(FileField):
    accepted_types = ['']

    def clean(self, *args, **kwargs):
        data = super(MIMECheckFileField, self).clean(*args, **kwargs)

        mimetype = data.content_type
        if mimetype not in self.accepted_types:
            raise ValidationError(self.error_messages['invalid'])

        return data


class DocumentFileField(MIMECheckFileField):

    default_error_messages = {
        'invalid': ugettext_lazy(
            "Uploaded file is not a document! \n Supported formats: doc/docx/xls/xlsx/odt/ods/txt"
        )
    }

    accepted_types = [
        'application/vnd.oasis.opendocument.text',  # .odt
        'application/vnd.oasis.opendocument.spreadsheet',  # .ods
        'application/vnd.ms-excel', 'application/msword', 'text/plain',  # .xls/.doc/.txt
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',  # .docx
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']  # .xlsx


@register_field_type('DynamicDocumentFileField')
class DynamicDocumentFileField(DynamicFileField):

    DESCRIPTION = ugettext_lazy("Attached document or spreadsheet")
    DJANGO_FIELD_TYPE = DocumentFileField


class ImageFileField(MIMECheckFileField):

    default_error_messages = {
        'invalid': ugettext_lazy(
            "Uploaded file is neither an image nor pdf!  \n Supported formats: gif/jpeg/png/svg/tiff/pdf"
        )
    }

    accepted_types = ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml',
                      'image/tiff', 'application/pdf']


@register_field_type('DynamicImageFileField')
class DynamicImageFileField(DynamicFileField):

    DESCRIPTION = ugettext_lazy("Attached image or pdf")
    DJANGO_FIELD_TYPE = ImageFileField


class DynamicChoicesFieldDataWrapper(BaseWrapper):
    def as_text(self):
        try:
            return dict(self.dynamic_field.get_django_field().choices)[self.data]
        except KeyError:
            return ugettext("Invalid key --- probably stale data in choice field")


@register_field_type("DynamicChoicesField")
class ChoicesField(_DjangoDynamicFieldController):

    DESCRIPTION = ugettext_lazy("ComboBox Field")
    DJANGO_FIELD_TYPE = ChoiceField
    WRAPPER = DynamicChoicesFieldDataWrapper

    def has_choice(self, dynamic_field, name):
        choices = [c.lower() for c in self.get_choices(dynamic_field)]
        return name.lower() in choices

    def get_choices(self, dynamic_field):
        """
        :param dynamic_field:
        :type dynamic_field: :class:`dynamic_forms.models.DynamicFormField`
        :return: List containing possible choices (strings)
        """
        if dynamic_field.additional_data is None:
            return []
        json_str = dynamic_field.additional_data.get("choices", "[]")
        return json.loads(json_str)

    def set_choices(self, dynamic_field, choices):

        choices = list(choices)

        for ch in choices:
            if not isinstance(ch, str):
                raise ValueError("Choices must be strings, {} is {}".format(ch, type(ch)))

        dynamic_field.additional_data['choices'] = choices

    def add_choice(self, dynamic_field, choice):

        if self.has_choice(dynamic_field, choice):
            raise ValueError("Field already contains choice '{}'".format(choice))

        choices = self.get_choices(dynamic_field)
        choices.append(choice)
        self.set_choices(dynamic_field, choices)

    def load_field(self, dynamic_field):
        choice_field = super().load_field(dynamic_field)
        choices = [(str(i), c) for i, c in enumerate(self.get_choices(dynamic_field))]
        if not dynamic_field.required:
            choices.insert(0, ('', "-" * 7))
        choice_field.choices = choices
        return choice_field

@register_field_type("DynamicTextField")
class DynamicTextField(_DjangoDynamicFieldController):

    DESCRIPTION = ugettext_lazy("Long text Field")
    DJANGO_FIELD_TYPE = forms.CharField

    def _create_field(self):
        field = super()._create_field()
        field.widget = Textarea()
        return field
