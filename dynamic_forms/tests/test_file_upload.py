# -*- coding: utf-8 -*-
import pathlib
from django.core.urlresolvers import reverse
from django.test import TestCase, override_settings
import unittest
from django.test.client import Client
from dynamic_forms.models import DynamicFormFile, DynamicForm, DynamicFormField

from .. import models

@override_settings(ROOT_URLCONF='dynamic_forms.test_urls', DEBUG=False)
class TestFilenameSanitizing(TestCase):


    def test_example_1(self):
        result = DynamicFormFile.clean_filename_for_user("Żaźółć Gęśką Jaźń")
        self.assertEqual(result, "_a____ G__k_ Ja__")

    def test_example_2(self):
        result = DynamicFormFile.clean_filename_for_user("Ala\tma kota.\nA Tola\t\nAsa")
        self.assertEqual(result, "Ala_ma kota__A Tola__Asa")

    def test_upload_file_with_long_field(self):

        form = DynamicForm()
        form.can_be_filled = True
        form.save()
        field = DynamicFormField(name="a"*101, field_type="DynamicFileField")
        form.add_field_to_form(field)
        field.save()

        c = Client()
        with (pathlib.Path(__file__).parent / 'example.file').open('rb') as f:
            response = c.post(
                reverse("dynamic_forms.views.fill_form", kwargs={
                    'dynamic_form_id': form.id
                }),
                data={
                    'b':c,
                    field.name: f
                }
            )

            self.assertEqual(response.status_code, 302)






