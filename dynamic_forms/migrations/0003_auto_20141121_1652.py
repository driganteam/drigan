# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dynamic_forms', '0002_auto_20141116_1454'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dynamicformfield',
            name='name',
            field=models.TextField(help_text='Name of the field, it will be displayed as label for this question', verbose_name='name'),
        ),
    ]
