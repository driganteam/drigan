from django.contrib import admin
from dynamic_forms.models import DynamicFormField, DynamicForm, DynamicFormData, \
    DynamicFormFile
import reversion


class DynamicFormAdmin(reversion.VersionAdmin):

    pass


admin.site.register(DynamicForm, DynamicFormAdmin)
admin.site.register(DynamicFormField, DynamicFormAdmin)
admin.site.register(DynamicFormData, DynamicFormAdmin)
admin.site.register(DynamicFormFile, DynamicFormAdmin)
