# -*- coding: utf-8 -*-

from django.forms import ChoiceField
from django.utils.translation import ugettext_lazy


class StopienInstruktorskiField(ChoiceField):

    stopien_choices = (
        ("Przewodniczka/Przewodnik", ugettext_lazy("Przewodniczka/Przewodnik")),
        ("Podharcmistrzyni/Podharcmistrz",
         ugettext_lazy("Podharcmistrzyni/Podharcmistrz")),
        ("Harcmistrzyni/Harcmistrz", ugettext_lazy("Harcmistrzyni/Harcmistrz")),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(self.stopien_choices, *args, **kwargs)


class StopienHarcerskiField(ChoiceField):

    stopien_choices = (
        ("Ochotniczka/Młodzik", ugettext_lazy("Ochotniczka/Młodzik")),
        ("Tropicielka/Wywiadowca", ugettext_lazy("Tropicielka/Wywiadowca")),
        ("Pionierka/Odkrywca", ugettext_lazy("Pionierka/Odkrywca")),
        ("Samarytanka/Ćwik", ugettext_lazy("Samarytanka/Ćwik")),
        ("Harcerka Orla/Harcerz Orli",
         ugettext_lazy("Harcerka Orla/Harcerz Orli")),
        ("Harcerka Rzeczypospolitej/Harcerz Rzeczypospolitej",
         ugettext_lazy("Harcerka Rzeczypospolitej/Harcerz Rzeczypospolitej")),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(self.stopien_choices, *args, **kwargs)
