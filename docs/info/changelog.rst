Lista zmian
===========


Wersja `0.1.11`
---------------

Drobne zmiany wzglęm 0.1.10

Wersja `0.1.10`
---------------

TODO


Wersja `0.1.7`
--------------

Data wydania: 07-10-2015

Naprawione błędy:

* REJCEN-269 Dodanie plików o nazwie ponad dwóch znaków powodowało błąd aplikacji
* REJCEN-305 Dodanie pola z plikiem o nazwie ponad 100 znaków powodowało błąd aplikajci


Wersja `0.1.6`
--------------

Data wydania: 05-09-2015

Naprawione blędy:

* Pola z nazwą zawierającą znak enter nie były możliwe do wypełnienia. Problem naprawiono.
* Pliki o nazwach dłuższych o niż 100 znaków powodowaly błąd aplikacji.
  Podniesiono limit do 2000 znaków oraz dodano komunikat o przyczynie błędu
  gdyby użytkownik ten limit naruszył.


Wersja `0.1.5`
--------------

Nowe funkcjonalności:

* Wymagane pola są oznaczone
*

Naprawiono błędy:

* Nazwy pól w formularzach są napisane z takim rozmiarem czcionki jak wprowadzono.
* Pola "Harcerskie" można ustawić jako nieobowiązkowę
* Właśniciel wydarzenia nie miał uprawnień do usuwania atrakcji. Problem naprawiono.
* Zmiany w kolejności wydarzeń na stronie głównej
* W rzadkich przypadkach nie dawało się pobrać listy uczestników. Problem naprawiono.
* Nie dawało się nie załączyć pliku do formularza. Problem naprawiono.

Wersja `0.1.3`
--------------

Nowe funkcjonalności:

* Przywrócono możliwość usunięcia Wydarzenia przez użytkownika
* Przywrócono możliwość usunięcia Atrakcji przez użytkownika
* Dodano pole na załącznik będący dokumentem i załącznik będący obrazkiem.
* Dodano guzki "Powrót" przy tworzeniu formularza ``REJCEN-215``

Naprawione błędy:
* Wyłączono pole z datą
* Naprawiono niedziałające pole ``Nip lub PESEL``
* Uniemożliwiono dodawanie pola ze spacją w nazwie
* Drobne usprawnienia w administracji



Wersja `0.1.1`
--------------

Naprawione błędy:

* W niektórych przypadkach (przy edycji pól z listą wybieraną) pliki csv
  nie generowały się w ogóle. Naprawiono ten błąd.


Wersja `0.1.0`
--------------

Nowe funkcjonalności

* Do każdego formularza dołożono pole z datą wypełnienia.
* Lista zgłoszeń jest sortowana względem kolejności zgłoszeń (powinno działać
  nawet dla starych zgłoszeń).

Naprawione błędy:

* Przy stworzeniu pola o długości większej niż 100 znaków pojawiał się
  błąd 500. Teraz pola nie mają ograniczenia.
* Nie dawało się wypełnić bardzo rozbudowanych formularzy z dużą ilością 
  tekstu (błąd 500). Problem został rozwiązany, pojedyńczy formularz
  może być teraz w zasadzie dowolnie duży.


Drobiazgi:

* Ukryto link do formularza administracyjnego dla osób które nie mają dostępu.